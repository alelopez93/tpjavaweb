<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h2>Lista de Zonas</h2>
		<table class="table table-striped">
			<thead>
				<th scope="row">#ID</th>
				<th scope="row">Nombre</th>
			</thead>
			<tbody>
				<c:forEach var="zona" items="${zonas}">
					<tr>

						<td>${zona.id}</td>
						<td>${zona.nombre}</td>
						<td><spring:url value="/zonas/${zona.id}/profes"
								var="updateURL" /> <a class="btn btn-primary"
							href="${updateURL }" role="button">Profesores</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<h2>Lista de Materias</h2>
		<table class="table table-striped">
			<thead>
				<th scope="row">#ID</th>
				<th scope="row">Nombre de Materia</th>				
			</thead>
			<tbody>
				<c:forEach var="materia" items="${materias}">
					<tr>
						<c:if test="${materia.id!=1}">
							<td>${materia.id}</td>
							<td>${materia.nombre}</td>
							<td><spring:url value="materias/${materia.id}/profes"
									var="updateURL" /> <a class="btn btn-primary"
								href="${updateURL }" role="button">Profesores</a></td>
							<td><spring:url
									value="materias/${materia.id}/problemasmateria" var="updateURL" />
								<a class="btn btn-primary" href="${updateURL }" role="button">Problemas</a></td>
						</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<h2>Lista de Estudiantes</h2>
		<table class="table table-striped">
			<thead>
				<th scope="row">#ID</th>
				<th scope="row">Nombre</th>
			</thead>
			<tbody>
				<c:forEach var="materia" items="${materias}">
					<tr>
						<c:if test="${materia.id==1}">
							<td>${materia.id}</td>
							<td>${materia.nombre}</td>
							<td><spring:url value="materias/${materia.id}/profes"
									var="updateURL" /> <a class="btn btn-primary"
								href="${updateURL }" role="button">Estudiantes</a></td>
						</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>