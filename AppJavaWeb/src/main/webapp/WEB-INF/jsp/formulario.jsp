<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<spring:url value="/save" var="saveURL" />
		<h2>Usuario</h2>
		<form:form modelAttribute="usuarioForm" method="post"
			action="${saveURL }" cssClass="form">
			<form:hidden path="id" />
			<div class="form-group">
				<label>Nombre</label>
				<form:input path="nombre" cssClass="form-control" id="nombre" />
			</div>
			<div class="form-group">
				<label>Apellido</label>
				<form:input path="apellido" cssClass="form-control" id="apellido" />
			</div>
			<div class="form-group">
				<label>Usser</label>
				<form:input path="usser" cssClass="form-control" id="usser" />
			</div>
			<div class="form-group">
				<label>Pass</label>
				<form:input path="pass" cssClass="form-control" id="pass" />
			</div>
			<div class="form-group">
				<label>Tipo de usario</label>
				<form:input path="tipoUsuario" cssClass="form-control"
					id="tipoUsuario" />
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</form:form>

	</div>
</body>
</html>