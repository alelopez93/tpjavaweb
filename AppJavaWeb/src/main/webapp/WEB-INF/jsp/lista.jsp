<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h2>Lista de Profesores</h2>
		<table class="table table-striped">
			<thead>
				<th scope="row">#ID</th>
				<th scope="row">Nombre</th>
				<th scope="row">Apellido</th>
				<th scope="row">Usser</th>
				<th scope="row">Pass</th>
				<th scope="row">Tipo de Usuario</th>
				<th scope="row">Tarifa</th>
			</thead>
			<tbody>
				<c:forEach var="usuario" items="${usuarios}">
					<tr>
						<td>${usuario.id}</td>
						<td>${usuario.nombre}</td>
						<td>${usuario.apellido}</td>
						<td>${usuario.usser}</td>
						<td>${usuario.pass}</td>
						<td>${usuario.tipoUsuario}</td>
						<td>${usuario.tarifa}</td>
						<td><spring:url value="/updateUsuario/${usuario.id }"
								var="updateURL" /> <a class="btn btn-primary"
							href="${updateURL }" role="button">Update</a> 
							<spring:url
								value="/usuarios/${usuario.id }/referencias" var="updateURL" />
							<a class="btn btn-primary" href="${updateURL}" role="button">Referencias</a>
						</td>
						<td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<spring:url value="/addUsuario" var="addURL" />
		<a class="btn btn-primary" href="${addURL }" role="button">Add New
			Usuario</a>
	</div>
</body>
</html>