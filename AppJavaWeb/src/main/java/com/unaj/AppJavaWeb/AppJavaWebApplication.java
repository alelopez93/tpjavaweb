package com.unaj.AppJavaWeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.unaj.AppJavaWeb")
public class AppJavaWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppJavaWebApplication.class, args);
	}
}
