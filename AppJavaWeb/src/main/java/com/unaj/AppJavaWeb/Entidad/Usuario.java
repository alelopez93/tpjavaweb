package com.unaj.AppJavaWeb.Entidad;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usuarios")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int id;

	private String nombre;

	private String apellido;

	private String usser;

	private String pass;

	@Column(name = "tarifa", nullable = true)
	private int tarifa;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipousuario", nullable = false)
	private TipoUsuario tipoUsuario;

	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
	private List<Referencia> referencias;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "UsuarioMateria", joinColumns = { @JoinColumn(name = "IdUsuario") }, inverseJoinColumns = {
			@JoinColumn(name = "IdMateria") })
	private Set<Materia> materias = new HashSet<Materia>();

	@ManyToOne
	@JoinColumn(name = "IdZona")
	private Zona zona;

	public Usuario() {

	}

	public Usuario(String nombre, String apellido, String usser, String pass, int tarifa, TipoUsuario tipoUsuario) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.usser = usser;
		this.pass = pass;
		this.tarifa = tarifa;
		this.tipoUsuario = tipoUsuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getUsser() {
		return usser;
	}

	public void setUsser(String usser) {
		this.usser = usser;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public int getTarifa() {
		return tarifa;
	}

	public void setTarifa(int tarifa) {
		this.tarifa = tarifa;
	}

	public List<Referencia> getReferencias() {
		return referencias;
	}

	public void setReferencias(List<Referencia> referencias) {
		this.referencias = referencias;
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", apellido=" + apellido + ", usser=" + usser + ", pass=" + pass
				+ ", tipoUsuario=" + tipoUsuario + "]";
	}

	public enum TipoUsuario {
		Estudiante, Profesor
	}

	/*
	 * public Set<Materia> getMaterias() { return materias; }
	 * 
	 * public void setMaterias(Set<Materia> materias) { this.materias = materias; }
	 */

}
