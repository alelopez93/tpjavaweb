package com.unaj.AppJavaWeb.Entidad;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "materias")
public class Materia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nombre;

	@OneToMany(mappedBy = "materia", cascade = CascadeType.ALL)
	public List<Problema> problemas;

	@ManyToMany(cascade = { CascadeType.ALL }, mappedBy = "materias")
	private Set<Usuario> profesores = new HashSet<Usuario>();

	public Materia() {
	}

	public Materia(String nombre) {
		super();
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Materia [id=" + id + ", nombre=" + nombre + "]";
	}

	public List<Problema> getProblemas() {
		return problemas;
	}

	public void setProblemas(List<Problema> problemas) {
		this.problemas = problemas;
	}

	public Set<Usuario> getProfesores() {
		return profesores;
	}

	public void setProfesores(Set<Usuario> profesores) {
		this.profesores = profesores;
	}

	/*
	 * public List<Usuario> getProfesores() { return profesores; }
	 * 
	 * public void setProfesores(List<Usuario> profesores) { this.profesores =
	 * profesores; }
	 */

}
