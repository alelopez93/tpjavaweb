package com.unaj.AppJavaWeb.Entidad;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "problemas")
public class Problema {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String enunciado;

	private String resolucion;

	@ManyToOne
	@JoinColumn(name = "IdMateria")
	private Materia materia;

	public Problema() {
		super();
	}

	public Problema(String enunciado, String resolucion) {
		super();
		this.enunciado = enunciado;
		this.resolucion = resolucion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

}
