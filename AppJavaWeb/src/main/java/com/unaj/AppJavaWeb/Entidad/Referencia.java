package com.unaj.AppJavaWeb.Entidad;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "referencias")
public class Referencia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String descripcion;

	@ManyToOne
	@JoinColumn(name = "IdUsuario")
	private Usuario usuario;

	public Referencia() {
	}

	public Referencia(String descripcion, Usuario usuario) {
		this.descripcion = descripcion;
		this.usuario = usuario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Referencia [descripcion=" + descripcion + "]";
	}

}
