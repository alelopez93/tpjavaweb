package com.unaj.AppJavaWeb.Entidad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "zonas")
public class Zona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nombre;

	@OneToMany(mappedBy = "zona", cascade = CascadeType.ALL)
	public List<Usuario> profesores;

	public Zona() {
		super();
	}

	public Zona(String nombre) {
		super();
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Usuario> getProfesores() {
		return profesores;
	}

	public void setProfesores(List<Usuario> profesores) {
		this.profesores = profesores;
	}

}
