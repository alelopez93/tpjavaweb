package com.unaj.AppJavaWeb.ServiceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unaj.AppJavaWeb.DAO.MateriaRepo;
import com.unaj.AppJavaWeb.Entidad.Materia;
import com.unaj.AppJavaWeb.Service.MateriaService;

@Service
public class MateriaServiceImp implements MateriaService {

	@Autowired
	MateriaRepo materiaRepo;

	@Override
	public List<Materia> getMaterias() {

		return (List<Materia>) materiaRepo.findAll();
	}

	@Override
	public Materia getMateria(Integer id) {
		return materiaRepo.findById(id).get();
	}

	@Override
	public void deleteMateria(Integer id) {
		materiaRepo.deleteById(id);

	}

	@Override
	public void saveMateria(Materia materia) {
		materiaRepo.save(materia);

	}

}
