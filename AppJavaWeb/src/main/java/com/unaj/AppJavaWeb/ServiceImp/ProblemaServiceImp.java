package com.unaj.AppJavaWeb.ServiceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unaj.AppJavaWeb.DAO.ProblemaRepo;
import com.unaj.AppJavaWeb.Entidad.Problema;
import com.unaj.AppJavaWeb.Service.ProblemaService;

@Service
public class ProblemaServiceImp implements ProblemaService {

	@Autowired
	ProblemaRepo problemaRepo;

	@Override
	public List<Problema> getProblemas() {
		return (List<Problema>) problemaRepo.findAll();
	}

	@Override
	public Problema getProblema(Integer id) {
		return problemaRepo.findById(id).get();
	}

	@Override
	public void deleteProblema(Integer id) {
		problemaRepo.deleteById(id);
	}

	@Override
	public void saveProblema(Problema problema) {
		problemaRepo.save(problema);
	}

}
