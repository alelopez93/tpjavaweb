package com.unaj.AppJavaWeb.ServiceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unaj.AppJavaWeb.DAO.ReferenciaRepo;
import com.unaj.AppJavaWeb.Entidad.Referencia;
import com.unaj.AppJavaWeb.Service.ReferenciaService;

@Service
public class ReferenciaServiceImp implements ReferenciaService {

	@Autowired
	ReferenciaRepo referenciaRepo;

	@Override
	public List<Referencia> getReferencias() {
		return (List<Referencia>) referenciaRepo.findAll();
	}

	@Override
	public Referencia getReferencia(Integer id) {
		return referenciaRepo.findById(id).get();
	}

	@Override
	public void deleteReferencia(Integer id) {
		referenciaRepo.deleteById(id);

	}

	@Override
	public void saveReferencia(Referencia referencia) {
		referenciaRepo.save(referencia);

	}

}
