package com.unaj.AppJavaWeb.ServiceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unaj.AppJavaWeb.DAO.ZonaRepo;
import com.unaj.AppJavaWeb.Entidad.Zona;
import com.unaj.AppJavaWeb.Service.ZonaService;

@Service
public class ZonaServiceImp implements ZonaService {

	@Autowired
	ZonaRepo zonaRepo;

	@Override
	public List<Zona> getZonas() {
		return (List<Zona>)zonaRepo.findAll();
	}

	@Override
	public Zona getZona(Integer id) {
		return zonaRepo.findById(id).get();
	}

	@Override
	public void deleteZona(Integer id) {
		zonaRepo.deleteById(id);
	}

	@Override
	public void saveZona(Zona zona) {
		zonaRepo.save(zona);

	}

}
