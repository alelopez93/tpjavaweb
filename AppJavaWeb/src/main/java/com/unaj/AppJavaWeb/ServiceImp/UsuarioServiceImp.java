package com.unaj.AppJavaWeb.ServiceImp;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unaj.AppJavaWeb.DAO.UsuarioRepo;
import com.unaj.AppJavaWeb.Entidad.Usuario;
import com.unaj.AppJavaWeb.Service.UsuarioService;

@Service
public class UsuarioServiceImp implements UsuarioService {

	@Autowired
	private UsuarioRepo usuarioRepo;

	@Override
	public List<Usuario> getUsuarios() {
		return (List<Usuario>) usuarioRepo.findAll();
	}

	@Override
	public void saveUsuario(Usuario usuario) {
		usuarioRepo.save(usuario);

	}

	@Override
	public Usuario getUsuario(Integer id) {
		return usuarioRepo.findById(id).get();
	}

	@Override
	public void deleteUsuario(Integer id) {
		usuarioRepo.deleteById(id);

	}

}
