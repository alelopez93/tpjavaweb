package com.unaj.AppJavaWeb.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.unaj.AppJavaWeb.Entidad.Referencia;
import com.unaj.AppJavaWeb.Service.ReferenciaService;

@RestController
public class ReferenciaController {

	@Autowired
	ReferenciaService referenciaService;

	// lista de referencias
	@GetMapping("/referencias")
	public Iterable<Referencia> getReferencias() {
		return referenciaService.getReferencias();
	}

	// referencia por id
	@GetMapping("/referencias/{id}")
	public Referencia getReferenciaPorID(@PathVariable int id) {
		return referenciaService.getReferencia(id);
	}

	// guardar referencia
	@PostMapping("/referencias")
	public Referencia addReferencia(@RequestBody Referencia referencia) {
		referenciaService.saveReferencia(referencia);
		return referencia;
	}

	// modificar referencia
	@PutMapping("/referencias")
	public Referencia updateReferencia(@RequestBody Referencia referencia) {
		referenciaService.saveReferencia(referencia);
		return referencia;
	}

	// borrar referencia por id
	@DeleteMapping("/referencias/{id}")
	public void deleteReferencia(@PathVariable int id) {
		referenciaService.deleteReferencia(id);
	}

}
