package com.unaj.AppJavaWeb.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.unaj.AppJavaWeb.Entidad.Materia;
import com.unaj.AppJavaWeb.Entidad.Problema;
import com.unaj.AppJavaWeb.Entidad.Referencia;
import com.unaj.AppJavaWeb.Entidad.Usuario;
import com.unaj.AppJavaWeb.Entidad.Zona;
import com.unaj.AppJavaWeb.ServiceImp.MateriaServiceImp;
import com.unaj.AppJavaWeb.ServiceImp.ProblemaServiceImp;
import com.unaj.AppJavaWeb.ServiceImp.ReferenciaServiceImp;
import com.unaj.AppJavaWeb.ServiceImp.UsuarioServiceImp;
import com.unaj.AppJavaWeb.ServiceImp.ZonaServiceImp;

@Controller
public class MainController2 {

	@Autowired
	UsuarioServiceImp usuarioService;
	@Autowired
	ReferenciaServiceImp referenciaService;
	@Autowired
	ZonaServiceImp zonaService;
	@Autowired
	MateriaServiceImp materiaService;
	@Autowired
	ProblemaServiceImp problemaService;

	@RequestMapping(value = "/addUsuario", method = RequestMethod.GET)
	public ModelAndView addArticle() {
		ModelAndView model = new ModelAndView();
		Usuario usuario = new Usuario();
		model.addObject("usuarioForm", usuario);
		model.setViewName("formulario");
		return model;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("formulario") Usuario usuario) {
		;
		usuarioService.saveUsuario(usuario);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/updateUsuario/{id}", method = RequestMethod.GET)
	public ModelAndView editArticle(@PathVariable int id) {
		ModelAndView model = new ModelAndView();
		Usuario usuario = usuarioService.getUsuario(id);
		model.addObject("usuarioForm", usuario);
		model.setViewName("formulario");
		return model;
	}

	@RequestMapping(value = "/usuarios/{id}/referencias", method = RequestMethod.GET)
	public ModelAndView listRefe(@PathVariable int id) {
		ModelAndView model = new ModelAndView("listaReferencias");
		Usuario usuario = usuarioService.getUsuario(id);
		List<Referencia> referenciaLista = usuario.getReferencias();
		model.addObject("referencias", referenciaLista);
		return model;

	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listZona() {
		ModelAndView model = new ModelAndView("listaZonas");
		List<Zona> zonaLista = zonaService.getZonas();
		List<Materia> materiasLista = materiaService.getMaterias();
		model.addObject("zonas", zonaLista);
		model.addObject("materias", materiasLista);
		return model;

	}

	@RequestMapping(value = "/zonas/{id}/profes", method = RequestMethod.GET)
	public ModelAndView listProfes(@PathVariable int id) {
		ModelAndView model = new ModelAndView("lista");
		Zona zona = zonaService.getZona(id);
		List<Usuario> profeZonas = zona.getProfesores();
		List<Usuario> usariosLista2 = new ArrayList<Usuario>();
		for (int i = 0; i < profeZonas.size(); i++) {
			Usuario actual = profeZonas.get(i);
			if (actual.getTipoUsuario().toString().equals("Profesor")) {
				usariosLista2.add(actual);
			}
		}
		model.addObject("usuarios", usariosLista2);
		return model;
	}

	@RequestMapping(value = "/materias/{id}/profes", method = RequestMethod.GET)
	public ModelAndView listProfesMat(@PathVariable int id) {
		ModelAndView model = new ModelAndView("lista");
		Materia materia = materiaService.getMateria(id);
		Set<Usuario> profeMaterias = materia.getProfesores();
		model.addObject("usuarios", profeMaterias);
		return model;
	}

	@RequestMapping(value = "/materias/{id}/problemasmateria", method = RequestMethod.GET)
	public ModelAndView listProfesProblem(@PathVariable int id) {
		ModelAndView model = new ModelAndView("listaProblemas");
		Materia materia = materiaService.getMateria(id);
		List<Problema> materiaProblemas = materia.getProblemas();
		model.addObject("problemas", materiaProblemas);
		return model;
	}

}
