package com.unaj.AppJavaWeb.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.unaj.AppJavaWeb.Entidad.Zona;
import com.unaj.AppJavaWeb.ServiceImp.ZonaServiceImp;

@RestController
public class ZonaController {

	@Autowired
	ZonaServiceImp zonaService;

	// lista de zonas
	@GetMapping("/zonas")
	public Iterable<Zona> getZonas() {
		return zonaService.getZonas();
	}

	// zona por id
	@GetMapping("/zonas/{id}")
	public Zona getZonaPorID(@PathVariable int id) {
		return zonaService.getZona(id);
	}

	// guardar zona
	@PostMapping("/zonas")
	public Zona addZonas(@RequestBody Zona zona) {
		zonaService.saveZona(zona);
		return zona;
	}

	// modificar zona
	@PutMapping("/zonas")
	public Zona updateZona(@RequestBody Zona zona) {
		zonaService.saveZona(zona);
		return zona;
	}

	// borrar zona por id
	@DeleteMapping("/zonas/{id}")
	public void deleteZona(@PathVariable int id) {
		zonaService.deleteZona(id);
	}

}
