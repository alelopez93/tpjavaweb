package com.unaj.AppJavaWeb.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.unaj.AppJavaWeb.Entidad.Materia;
import com.unaj.AppJavaWeb.Service.MateriaService;

@RestController
public class MateriaController {

	@Autowired
	MateriaService materiaService;

	// lista de materias
	@GetMapping("/materias")
	public List<Materia> getMaterias() {
		return materiaService.getMaterias();
	}

	// materia por id
	@GetMapping("/materias/{id}")
	public Materia getMateriaPorID(@PathVariable int id) {
		return materiaService.getMateria(id);
	}

	// guardar materia
	@PostMapping("/materias")
	public Materia addMateria(@RequestBody Materia materia) {
		materiaService.saveMateria(materia);
		return materia;
	}

	// modificar materia
	@PutMapping("/materias")
	public Materia updateMateria(@RequestBody Materia materia) {
		materiaService.saveMateria(materia);
		return materia;
	}

	// borrar materia
	@DeleteMapping("/materias/{id}")
	public void deleteMateria(@PathVariable int id) {
		materiaService.deleteMateria(id);
	}
}
