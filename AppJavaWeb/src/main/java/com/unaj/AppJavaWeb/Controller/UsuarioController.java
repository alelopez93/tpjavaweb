package com.unaj.AppJavaWeb.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.unaj.AppJavaWeb.Entidad.Usuario;
import com.unaj.AppJavaWeb.ServiceImp.UsuarioServiceImp;

@RestController
public class UsuarioController {

	@Autowired
	UsuarioServiceImp usuarioService;

	// lista de usuarios
	@GetMapping("/usuarios")
	public Iterable<Usuario> getUsuarios() {
		return usuarioService.getUsuarios();
	}

	// usuario por id
	@GetMapping("/usuarios/{id}")
	public Usuario getUsuariosPorID(@PathVariable int id) {
		return usuarioService.getUsuario(id);
	}

	// guardar usuario
	@PostMapping("/usuarios")
	public Usuario addUsuario(@RequestBody Usuario usuario) {
		usuarioService.saveUsuario(usuario);
		return usuario;
	}

	// modificar usuario
	@PutMapping("/usuarios")
	public Usuario updateUsuario(@RequestBody Usuario usuario) {
		usuarioService.saveUsuario(usuario);
		return usuario;
	}

	// borrar usuario por id
	@DeleteMapping("/usuarios/{id}")
	public void deleteUsuario(@PathVariable int id) {
		usuarioService.deleteUsuario(id);
	}

}
