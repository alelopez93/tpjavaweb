package com.unaj.AppJavaWeb.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.unaj.AppJavaWeb.Entidad.Problema;
import com.unaj.AppJavaWeb.ServiceImp.ProblemaServiceImp;

@RestController
public class ProblemaController {

	@Autowired
	ProblemaServiceImp problemaService;

	// lista de problemas
	@GetMapping("/problemas")
	public Iterable<Problema> getProblemas() {
		return problemaService.getProblemas();
	}

	// problema por id
	@GetMapping("/problemas/{id}")
	public Problema getProblemaPorID(@PathVariable int id) {
		return problemaService.getProblema(id);
	}

	// guardar problema
	@PostMapping("/problemas")
	public Problema addProblema(@RequestBody Problema problema) {
		problemaService.saveProblema(problema);
		return problema;
	}

	// modificar problema
	@PutMapping("/problemas")
	public Problema updateProblema(@RequestBody Problema problema) {
		problemaService.saveProblema(problema);
		return problema;
	}

	// borrar problema por id
	@DeleteMapping("/problemas/{id}")
	public void deleteProblema(@PathVariable int id) {
		problemaService.deleteProblema(id);
	}
}
