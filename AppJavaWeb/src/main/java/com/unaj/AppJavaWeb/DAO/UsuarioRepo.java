package com.unaj.AppJavaWeb.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unaj.AppJavaWeb.Entidad.*;

public interface UsuarioRepo extends JpaRepository<Usuario, Integer> {

}
