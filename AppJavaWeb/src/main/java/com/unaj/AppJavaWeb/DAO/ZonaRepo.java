package com.unaj.AppJavaWeb.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unaj.AppJavaWeb.Entidad.Zona;

public interface ZonaRepo extends JpaRepository<Zona, Integer> {

}
