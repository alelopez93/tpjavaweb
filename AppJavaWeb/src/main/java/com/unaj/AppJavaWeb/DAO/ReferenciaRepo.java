package com.unaj.AppJavaWeb.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unaj.AppJavaWeb.Entidad.Referencia;

public interface ReferenciaRepo extends JpaRepository<Referencia, Integer> {

}
