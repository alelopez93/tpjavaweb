package com.unaj.AppJavaWeb.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unaj.AppJavaWeb.Entidad.Problema;

public interface ProblemaRepo extends JpaRepository<Problema, Integer> {

}
