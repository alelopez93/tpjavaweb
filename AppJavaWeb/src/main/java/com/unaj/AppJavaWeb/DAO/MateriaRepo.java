package com.unaj.AppJavaWeb.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unaj.AppJavaWeb.Entidad.Materia;

public interface MateriaRepo extends JpaRepository<Materia, Integer> {

}
