package com.unaj.AppJavaWeb.Service;

import java.util.List;
import com.unaj.AppJavaWeb.Entidad.Usuario;

public interface UsuarioService {

	public List<Usuario> getUsuarios();

	public void saveUsuario(Usuario usuario);

	public Usuario getUsuario(Integer id);

	public void deleteUsuario(Integer id);

}
