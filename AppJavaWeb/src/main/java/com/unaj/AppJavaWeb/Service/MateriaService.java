package com.unaj.AppJavaWeb.Service;

import java.util.List;

import com.unaj.AppJavaWeb.Entidad.Materia;

public interface MateriaService {

	public List<Materia> getMaterias();

	public Materia getMateria(Integer id);

	public void deleteMateria(Integer id);

	public void saveMateria(Materia materia);

}
