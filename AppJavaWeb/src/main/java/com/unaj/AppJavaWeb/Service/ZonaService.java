package com.unaj.AppJavaWeb.Service;

import java.util.List;

import com.unaj.AppJavaWeb.Entidad.Zona;

public interface ZonaService {

	public List<Zona> getZonas();

	public Zona getZona(Integer id);

	public void deleteZona(Integer id);

	public void saveZona(Zona zona);

}
