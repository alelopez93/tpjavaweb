package com.unaj.AppJavaWeb.Service;

import java.util.List;

import com.unaj.AppJavaWeb.Entidad.Problema;

public interface ProblemaService {

	public List<Problema> getProblemas();

	public Problema getProblema(Integer id);

	public void deleteProblema(Integer id);

	public void saveProblema(Problema problema);

}
