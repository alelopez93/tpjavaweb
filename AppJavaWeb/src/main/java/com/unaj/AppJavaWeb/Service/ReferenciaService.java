package com.unaj.AppJavaWeb.Service;

import java.util.List;

import com.unaj.AppJavaWeb.Entidad.Referencia;

public interface ReferenciaService {

	public List<Referencia> getReferencias();

	public Referencia getReferencia(Integer id);

	public void deleteReferencia(Integer id);

	public void saveReferencia(Referencia referencia);

}
