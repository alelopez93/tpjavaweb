insert into zonas(id,nombre) values (1,'berazategui');
insert into zonas(id,nombre) values (2,'varela');

insert into materias(id,nombre) values (1,'ninguna');
insert into materias(id,nombre) values (2,'matematica');
insert into materias(id,nombre) values (3,'literatura');

insert into problemas(id,enunciado,resolucion,ID_MATERIA) values (1,'suma','resolucion de suma',2);
insert into problemas(id,enunciado,resolucion,ID_MATERIA) values (2,'resta','resolucion de resta',2);
insert into problemas(id,enunciado,resolucion,ID_MATERIA) values (3,'oraciones','resolucion de oraciones',3);
insert into problemas(id,enunciado,resolucion,ID_MATERIA) values (4,'comprension de texto','resolucion de comprension de texto',3);

insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(1,'ale','lopez','aleee','1234','Estudiante',1,0);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(2,'juan','perez','juan','1111','Profesor',1,200);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(3,'mario','gomez','mario','2222','Estudiante',2,0);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(4,'facundo','diaz','facu','3333','Profesor',1,300);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(5,'luciano','gonzalez','lu','4444','Estudiante',2,0);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(6,'mariano','gomez','marian','3333','Profesor',1,400);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(7,'mario','diaz','mario123','2222','Profesor',1,500);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(8,'facundo','gonzalez','facu456','3333','Profesor',2,250);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(9,'luciano','fernandez','lu789','4444','Profesor',2,350);
insert into usuarios (id,nombre,apellido,usser,pass,tipoUsuario,ID_ZONA,tarifa)values(10,'juan','fernandez','marian456','3333','Profesor',2,150);

insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (1,1);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (3,1);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (5,1);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (2,2);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (4,2);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (6,2);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (7,2);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (8,3);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (9,3);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (10,3);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (2,3);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (4,3);
insert into USUARIO_MATERIA(ID_USUARIO,ID_MATERIA) values (7,3);



insert into referencias(id,descripcion, ID_USUARIO) values (1,'excelente',2);
insert into referencias(id,descripcion, ID_USUARIO) values (6,'muy bueno',2);
insert into referencias(id,descripcion, ID_USUARIO) values (4,'muy bueno',2);
insert into referencias(id,descripcion, ID_USUARIO) values (8,'muy bueno',6);
insert into referencias(id,descripcion, ID_USUARIO) values (2,'bueno',6);
insert into referencias(id,descripcion, ID_USUARIO) values (3,'malo',4);
insert into referencias(id,descripcion, ID_USUARIO) values (5,'malo',4);
insert into referencias(id,descripcion, ID_USUARIO) values (7,'malo',4);
insert into referencias(id,descripcion, ID_USUARIO) values (9,'muy malo',7);
insert into referencias(id,descripcion, ID_USUARIO) values (10,'regular',7);
insert into referencias(id,descripcion, ID_USUARIO) values (11,'malo',8);
insert into referencias(id,descripcion, ID_USUARIO) values (12,'malo',8);
insert into referencias(id,descripcion, ID_USUARIO) values (13,'muy malo',8);
insert into referencias(id,descripcion, ID_USUARIO) values (14,'regular',9);
insert into referencias(id,descripcion, ID_USUARIO) values (15,'malo',10);
insert into referencias(id,descripcion, ID_USUARIO) values (16,'malo',10);
insert into referencias(id,descripcion, ID_USUARIO) values (17,'muy malo',10);
insert into referencias(id,descripcion, ID_USUARIO) values (18,'regular',10);
insert into referencias(id,descripcion, ID_USUARIO) values (19,'regular',10);
insert into referencias(id,descripcion, ID_USUARIO) values (20,'regular',10);





